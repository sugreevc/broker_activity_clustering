import pandas as pd
import numpy as np

from .time_series_feature_extraction import feature_extraction_transformer

# def validate_data(data:pd.DataFrame):
#     pass


def filter_data(
    data: pd.DataFrame,
    earliest_quote_date: str,
    latest_quote_date: str,
    earliest_activation_date: str,
    latest_activation_date: str,
) -> pd.DataFrame:

    """
    Apply date filters to data
    "activation_week", "quote_week", "producer_code", "n_quotes"
    """
    quote_min = pd.to_datetime(earliest_quote_date) or data["quote_week"].min()
    quote_max = pd.to_datetime(latest_quote_date) or data["quote_week"].max()
    activation_min = (
        pd.to_datetime(earliest_activation_date) or data["activation_week"].min()
    )
    activation_max = (
        pd.to_datetime(latest_activation_date) or data["activation_week"].max()
    )

    return data[
        (data["quote_week"] >= quote_min)
        & (data["quote_week"] <= quote_max)
        & (data["activation_week"] >= activation_min)
        & (data["activation_week"] <= activation_max)
    ]


def ts_feature_extraction(df: pd.DataFrame) -> pd.DataFrame:
    """Extract timeseries features"""

    fdf = feature_extraction_transformer.transform(df)

    return fdf


def select_cluster_cols(fdf: pd.DataFrame) -> np.ndarray:
    scaled_feature_cols = [f for f in fdf.columns.tolist() if "scaled" in f]
    X = fdf[scaled_feature_cols].values
    return X
