from kedro.pipeline import Pipeline, node

from .nodes import filter_data, ts_feature_extraction, select_cluster_cols


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=filter_data,
                inputs=[
                    "quotes_per_week_2021-07-12_small_retail",
                    "params:earliest_quote_date",
                    "params:latest_quote_date",
                    "params:earliest_activation_date",
                    "params:latest_activation_date",
                ],
                outputs="filtered_data_df",
                name="filter_data",
            ),
            node(
                func=ts_feature_extraction,
                inputs="filtered_data_df",
                outputs="ts_features_df",
                name="ts_feature_extract",
            ),
            node(
                func=select_cluster_cols,
                inputs="ts_features_df",
                outputs="cluster_cols_array",
                name="make_cluster_cols_array",
            ),
        ]
    )
