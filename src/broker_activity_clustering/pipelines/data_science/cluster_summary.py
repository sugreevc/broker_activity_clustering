import pandas as pd
from typing import List


def cluster_features_describe(
    df_labeled: pd.DataFrame, labels_col: str, statistic: str, feature_cols: List[str]
) -> pd.DataFrame:
    """
    Given dataframe of with n_samples rows, a column of
    cluster labels, & n_features columns, return a (n_clusters x n_features) 
    dataframe with index=cluster number and columns are the requested
    statistic (as calculated by .describe(), mean/min/etc) of each cluster
    feature distribution.
    """
    return (
        df_labeled.groupby(labels_col)[feature_cols]
        .describe()
        .xs(key=statistic, axis=1, level=1)
    )


def calc_cluster_cardinality(df_labeled: pd.DataFrame, labels_col: str) -> pd.DataFrame:
    """
    Given dataframe of with n_samples rows and a column of
    cluster labels, return a (n_clusters x 2) dataframe with cluster
    number index and columns: number of samples 
    & fraction of total samples per cluster.
    """
    vc = df_labeled[labels_col].value_counts()
    merged = pd.merge(vc, round(vc / vc.sum(), 2), left_index=True, right_index=True)
    return merged.rename(
        columns=dict(zip(merged.columns, ["n_samples", "fraction_total"]))
    )
