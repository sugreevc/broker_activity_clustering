from kedro.pipeline import Pipeline, node

from .nodes import kmeans_cluster, summarize_clusters


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=kmeans_cluster,
                inputs=[
                    "cluster_cols_array",
                    "ts_features_df",
                    "params:num_clusters",
                    "params:random_state",
                    "params:n_init",
                    "params:max_iter",
                ],
                outputs="labeled_features_df",
                name="cluster",
            ),
            node(
                func=summarize_clusters,
                inputs=["labeled_features_df", "params:num_clusters"],
                outputs="cluster_summary_df",
                name="summarize_clusters",
            ),
        ]
    )
