# pylint: disable=invalid-name
import numpy as np
import pandas as pd

from .cluster_summary import calc_cluster_cardinality, cluster_features_describe
from sklearn.cluster import KMeans


def kmeans_cluster(
    X: np.ndarray,
    fdf: pd.DataFrame,
    n_clusters: int,
    random_state: int,
    n_init: int,
    max_iter: int,
):
    """
    Run k-Means on selected feature columns (X) and add labels column to 
    features dataframe.
    """
    km = KMeans(
        n_clusters=n_clusters,
        random_state=random_state,
        n_init=n_init,
        max_iter=max_iter,
    )
    k_key = "k" + str(n_clusters)
    fdf[k_key] = km.fit_predict(X)
    return fdf


def summarize_clusters(fdf: pd.DataFrame, num_clusters: int):

    k_key = "k" + str(num_clusters)
    desc_cols = [f for f in fdf.columns.tolist() if (f not in ("producer_code", k_key))]
    kdf = pd.concat(
        [
            calc_cluster_cardinality(fdf, k_key),
            cluster_features_describe(fdf, k_key, "mean", desc_cols),
            # cluster_features_describe(fdf, k_key, "std", feature_cols),
        ],
        axis=1,
    )

    return kdf
