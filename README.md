# broker_activity_clustering

How to use:  

Download quote activity data as CSV (unformatted, no vizualizations applied) to `/data/01_raw` from this Looker [Explore](https://attune.looker.com/explore/Attune_Production/quotes_v4?qid=CT4SjsV9h7n0MH2N7HTC9G&toggle=fil) and update the Data Catalog (see `conf/base/catalog.yml`) with the new dataset name/params. If you change the column order in Looker before export, be sure to change the column order in `load_args` in `conf/base/catalog.yml`.  

Note filters on quote bundle type, activation date, quote date, broker segment, etc should be applied in Looker before export since no additional queries are made to DW in this repo. Further filters for specific quote date and activation date ranges for clustering available as kedro parameters (set in `parameters.yml` or via command line as `kedro run` flags):

Params available:  
- earliest_quote_date: None
- latest_quote_date: None
- num_clusters: 3 - number of activity clusters to use in k-means alogrithm
- earliest_activation_date: None
- latest_activation_date: None

Date params set to `None` will not do any filtering, i.e. cluster on entire downloaded dataset.

This script can be run with specific parameters defined in `parameters.yml` as (example):  
`kedro run --params earliest_quote_date:2021-04-01,latest_quote_date:2021-07-10,earliest_activation_date:2020-10-01,latest_activation_date:2020-12-31`

Outputs of the data processing pipeline (features, model outputs) are also defined in the Data Catalog.

# Boilerplate Kedro docs

This is your new Kedro project, which was generated using `Kedro 0.17.3`.

Take a look at the [Kedro documentation](https://kedro.readthedocs.io) to get started.

## Rules and guidelines

In order to get the best out of the template:

* Don't remove any lines from the `.gitignore` file we provide
* Make sure your results can be reproduced by following a [data engineering convention](https://kedro.readthedocs.io/en/stable/11_faq/01_faq.html#what-is-data-engineering-convention)
* Don't commit data to your repository
* Don't commit any credentials or your local configuration to your repository. Keep all your credentials and local configuration in `conf/local/`

## How to install dependencies

Declare any dependencies in `src/requirements.txt` for `pip` installation and `src/environment.yml` for `conda` installation.

To install them, run:

```
kedro install
```

## How to run Kedro

You can run your Kedro project with:

```
kedro run
```

## How to test your Kedro project

Have a look at the file `src/tests/test_run.py` for instructions on how to write your tests. You can run your tests as follows:

```
kedro test
```

To configure the coverage threshold, look at the `.coveragerc` file.


## Project dependencies

To generate or update the dependency requirements for your project:

```
kedro build-reqs
```

This will copy the contents of `src/requirements.txt` into a new file `src/requirements.in` which will be used as the source for `pip-compile`. You can see the output of the resolution by opening `src/requirements.txt`.

After this, if you'd like to update your project requirements, please update `src/requirements.in` and re-run `kedro build-reqs`.

[Further information about project dependencies](https://kedro.readthedocs.io/en/stable/04_kedro_project_setup/01_dependencies.html#project-specific-dependencies)

## How to work with Kedro and notebooks

> Note: Using `kedro jupyter` or `kedro ipython` to run your notebook provides these variables in scope: `context`, `catalog`, and `startup_error`.

### Jupyter
To use Jupyter notebooks in your Kedro project, you need to install Jupyter:

```
pip install jupyter
```

After installing Jupyter, you can start a local notebook server:

```
kedro jupyter notebook
```

### JupyterLab
To use JupyterLab, you need to install it:

```
pip install jupyterlab
```

You can also start JupyterLab:

```
kedro jupyter lab
```

### IPython
And if you want to run an IPython session:

```
kedro ipython
```

### How to convert notebook cells to nodes in a Kedro project
You can move notebook code over into a Kedro project structure using a mixture of [cell tagging](https://jupyter-notebook.readthedocs.io/en/stable/changelog.html#cell-tags) and Kedro CLI commands.

By adding the `node` tag to a cell and running the command below, the cell's source code will be copied over to a Python file within `src/<package_name>/nodes/`:

```
kedro jupyter convert <filepath_to_my_notebook>
```
> *Note:* The name of the Python file matches the name of the original notebook.

Alternatively, you may want to transform all your notebooks in one go. Run the following command to convert all notebook files found in the project root directory and under any of its sub-folders:

```
kedro jupyter convert --all
```

### How to ignore notebook output cells in `git`
To automatically strip out all output cell contents before committing to `git`, you can run `kedro activate-nbstripout`. This will add a hook in `.git/config` which will run `nbstripout` before anything is committed to `git`.

> *Note:* Your output cells will be retained locally.

## Package your Kedro project

[Further information about building project documentation and packaging your project](https://kedro.readthedocs.io/en/stable/03_tutorial/05_package_a_project.html)